import React, {useState} from "react";

export default function TextForm(props) {
    const handleUpClick = () =>{
        console.log("Upper Case was Clicked")
        let newText = text.toUpperCase();
        setText(newText)
        props.showAlert("Text converted to Upper Case", "success")
    }

    const handleLoClick = () =>{
        console.log("Lower Case was Clicked")
        let newText = text.toLowerCase();
        setText(newText)
        props.showAlert("Text converted to Lower Case", "success")
    }

    const handleClearClick = () =>{
        console.log("Clear Text was Clicked")
        let newText = ("");
        setText(newText)
        props.showAlert("Text has been cleared", "success")
    }

    const handleOnChange = (event) =>{
        console.log("Handle on Change was Clicked");
        setText(event.currentTarget.value);
    }

    const [text, setText] = useState();
    
    return (
        <>
        <div className="container" style = {{color: props.mode==='dark' ? 'white' : '#504e4e'}}>
            <h1 >{props.heading} </h1>
            <div className="mb-3">
                <textarea
                    className="form-control"
                    value={text}
                    onChange={handleOnChange}
                    style = {{backgroundColor: props.mode==='dark' ? 'grey' : 'white', color: props.mode==='dark' ? 'white' : '#504e4e'}}
                    id="myBox"
                    rows="8"              
                ></textarea>
            </div>
            <button className="btn btn-primary mx-2" onClick={handleUpClick}>Convert to UpperCase</button>
            <button className="btn btn-primary mx-2" onClick={handleLoClick}>Convert to LowerCase</button>
            <button className="btn btn-primary mx-2" onClick={handleClearClick}>Clear the Text</button>
        </div>
        <div className="container" style = {{color: props.mode==='dark' ? 'white' : '#504e4e'}} >
            <h1>You text summary</h1>
            <p>300 words, 123456 characters</p>
            {/* <p>{text.split(" ").length} words and {text.length} characters</p> */}
            {/* <p>{0.008 * text.split(" ").length} Minutes to read</p> */}
            
        </div>
        </>
    );
}
